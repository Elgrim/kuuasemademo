﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    [Header("Speed values")]
    [SerializeField]
    private float acceleration = 1.0f;
    [SerializeField]
    private float deacceleration = 1.0f;

    [SerializeField]
    private float maxSpeed = 10.0f;

    [SerializeField]
    private Vector2 heightConstraints;

    private float horizontal = 0f;
    private float vertical = 0f;

    public Rigidbody2D rb;
    public SpriteRenderer shipSprite;

    void FixedUpdate()
    {
        Move();
    }

    void Move()
    {
        //Clamp height to level limits
        if (transform.position.y <= heightConstraints.x && vertical <= 0)
        {
            vertical = 0;
        }
        else if (transform.position.y >= heightConstraints.y && vertical >= 0)
        {
            vertical = 0;
        }
        //Calc velocity
        Vector2 velocity = new Vector2(horizontal, vertical);

        //Move rigidbody
        rb.MovePosition(rb.position + velocity * Time.deltaTime);

        //If we are not getting imputs have velocity slow down
        if (Input.GetAxis("Horizontal") == 0 && horizontal != 0) horizontal = Mathf.MoveTowards(horizontal, 0, deacceleration * Time.deltaTime);
        if (Input.GetAxis("Vertical") == 0 && vertical != 0) vertical = Mathf.MoveTowards(vertical, 0, deacceleration * Time.deltaTime);
    }

    public void AddHorizontal(float value) {
        horizontal = NewAccelValue(horizontal, value);
        //Make the ship to face move direction
        shipSprite.flipX = (horizontal < 0);
    }

    public void AddVertical(float value)
    {
        vertical = NewAccelValue(vertical, value);
    }

    //Calculates new acceleration values for velocity
    float NewAccelValue(float currentValue, float dirVal) {
        float newVal = currentValue + (acceleration * Time.deltaTime) * dirVal;
        newVal = Mathf.Clamp(newVal, -maxSpeed, maxSpeed);
        return newVal;
    }
}
