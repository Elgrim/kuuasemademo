﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

    public delegate void ScoreChanged(int newScore);
    public static event ScoreChanged OnScoreIncrease;
    public static event ScoreChanged OnHiScoreIncrease;

    private int currentScore;

    private void Start()
    {
        HighScore = GameController.instance.hiScore;
        if (OnHiScoreIncrease != null) OnHiScoreIncrease(HighScore);
    }

    public void ResetScore() {
        currentScore = 0;
    }

    public int HighScore { get; set; }

    public void AddScore(int scoreToAdd) {
        currentScore += scoreToAdd;
        if (OnScoreIncrease != null) OnScoreIncrease(currentScore);
        CheckForHighScore();
    }

    void CheckForHighScore() {
        if (currentScore > HighScore)
        {
            HighScore = currentScore;
            if (OnHiScoreIncrease != null) OnHiScoreIncrease(HighScore);
        }
    }
}
