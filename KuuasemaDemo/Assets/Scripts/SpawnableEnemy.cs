﻿
using UnityEngine;

[System.Serializable]
public class SpawnableEnemy
{
    public GameObject prefab;
    [SerializeField]
    private int startSpawnChance;
    private int currentChance;
    [SerializeField]
    private int maxSpawnChance;
    [SerializeField]
    private int spawnChanceInc;

    private int id;

    public void SetID(int newID)
    {
        id = newID;
    }

    public void ResetSpawnChange()
    {
        currentChance = startSpawnChance;
    }

    public void IncreaseSpawnChange()
    {
        currentChance += spawnChanceInc;
        if (currentChance >= maxSpawnChance)
        {
            currentChance = maxSpawnChance;
        }
    }

    public int GetCurrentSpawnChance()
    {
        return currentChance;
    }

    public int GetCurrentID()
    {
        return id;
    }

}
