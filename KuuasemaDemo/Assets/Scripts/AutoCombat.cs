﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoCombat : MonoBehaviour {

    private bool autoCombatActive;

    public PlayerMovement pMovement;
    public Shoot shootScript;

    public GameObject player;  

    //Human references
    public List<Human> kidnappedHumans;
    public Human targetHumanScript;
    public bool savingHuman;

    //Autocombat scripts
    private AutoCombat_Movement ac_Movement;
    private AutoCombat_Detection ac_Detection;
    private AutoCombat_EnemyHandler ac_EnemyHandler;

    // Use this for initialization
    void Start () {
        shootScript = player.GetComponent<Shoot>();
        ac_Movement = gameObject.GetComponent<AutoCombat_Movement>();
        ac_Movement.SetUp(this);
        ac_Detection = gameObject.GetComponent<AutoCombat_Detection>();
        ac_Detection.SetUp(this);
        ac_EnemyHandler = gameObject.GetComponent<AutoCombat_EnemyHandler>();
        ac_EnemyHandler.SetUp(this);
    }

    public void ToggleAutoCombat(bool state) {
        autoCombatActive = state;
       player.GetComponent<PlayerInputHandler>().AutocombatStatus(autoCombatActive);
    }
	
	//Update is called once per frame
	void Update () {
        if (autoCombatActive)
        {
            RunAutoCombat();
        }
	}

    void RunAutoCombat() {
        transform.position = player.transform.position;
        if (kidnappedHumans.Count > 0)
        {
            if (!savingHuman)
            {
                targetHumanScript = kidnappedHumans[0];
                savingHuman = true;
            }
            SaveHuman();
        }

        if (ac_EnemyHandler.GetEnemyList().Count > 0)
        {
            ac_Detection.CheckIfEnemiesInFrontOfTheShip();
            ac_EnemyHandler.FindAndShootEnemy();
        }
        else
        {
            if (!savingHuman) ac_Movement.MoveHorizontally(1);
        }
    }

    void OnEnable()
    {
        Human.OnHumanKidnapped += HumanKidnapped;
    }

    void OnDisable()
    {
        Human.OnHumanKidnapped -= HumanKidnapped;
    }

    //Called vie delegate when human has been captured
    void HumanKidnapped(Human newHumanScript) {
        kidnappedHumans.Add(newHumanScript);
    }

    void SaveHuman() {
        HumanState state = targetHumanScript.GetCurrentState();
        switch (state)
        {
            case HumanState.Grounded:
                ClearHumanTargetRef(targetHumanScript);
                break;
            case HumanState.Kidnapped:
                ac_Movement.MoveToTargetDir(targetHumanScript.transform, 0);
                ac_Movement.MoveToTargetLevel(targetHumanScript.transform, -1);
                break;
            case HumanState.Falling:
                ac_Movement.MoveToTargetDir(targetHumanScript.transform, 0);
                ac_Movement.MoveToTargetLevel(targetHumanScript.transform, -1);
                break;
            case HumanState.Saved:
                ac_Movement.MoveVertically(-1);
                break;
            case HumanState.Dead:
                ClearHumanTargetRef(targetHumanScript);
                break;
        }
    }

    void ClearHumanTargetRef(Human scriptToRemove) {
        kidnappedHumans.Remove(targetHumanScript);
        targetHumanScript = null;
        savingHuman = false;
    }

    public bool IsAutoCombatActive() {
        return autoCombatActive;
    }

}
