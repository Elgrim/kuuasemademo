﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public SpawnableEnemy[] enemiesToSpawn;

    [SerializeField]
    private float timeBetweenSpawns = 1f;
    private float currentCooldown = 0f;
    private float cooldownTickSpeed = 0.02f;
    private int currentTotalSpawnWeight;

    private int chosenSpawnIndex = 0;  //Used to determine index value of spawnable enemy in deactive enemies list

    [SerializeField]
    private int maxSpawns = 5;

    private List<Enemy> activeEnemies = new List<Enemy>();
    private List<Enemy> deactiveEnemies = new List<Enemy>();

    private Vector2 spawnXLimit;
    private Vector2 spawnYLimit;

    // Use this for initialization
    public void SetUp()
    {
        for (int i = 0; i < enemiesToSpawn.Length; i++)
        {
            enemiesToSpawn[i].SetID(i);
            enemiesToSpawn[i].ResetSpawnChange();
        }
        CalculateNewTotalWeighForSpawn();
        for (int i = 0; i < maxSpawns; i++)
        {
            SpawnRandomEnemy();
        }
        //Set cooldown to default value
        currentCooldown = timeBetweenSpawns;
        //Start spawning coroutine
        StartCoroutine(SpawnEnemies());
    }

    private void OnEnable()
    {
        DifficultyHandler.OnDifficultyIncreased += IncreaseDifficulty;
    }

    private void OnDisable()
    {
        DifficultyHandler.OnDifficultyIncreased -= IncreaseDifficulty;
    }

    void IncreaseDifficulty() {
        maxSpawns++;
        timeBetweenSpawns -= 0.1f;
        timeBetweenSpawns = Mathf.Clamp(timeBetweenSpawns, 0.5f, 2f);
        for (int i = 0; i < enemiesToSpawn.Length; i++)
        {
            enemiesToSpawn[i].IncreaseSpawnChange();
        }
    }

    //Set limits of what range enemies can spawn in the world
    public void SetSpawnLimits(Vector2 xLimit, Vector2 yLimit) {
        spawnXLimit = xLimit;
        spawnYLimit = yLimit;
    }

    IEnumerator SpawnEnemies()
    {
        while (true)
        {
            if (currentCooldown != 0)
            {
                currentCooldown -= cooldownTickSpeed;

                if (currentCooldown <= 0)
                {
                    //Cooldown finished
                    //Spawn new enemy if we are not at max spawn amount
                    if (activeEnemies.Count < maxSpawns)
                    {
                        SpawnRandomEnemy();
                    }
                    
                }
            }

            yield return new WaitForSeconds(cooldownTickSpeed);
        }

    }

    void SpawnRandomEnemy() {
        //Refresh cooldown
        currentCooldown = timeBetweenSpawns;
        //Get random index from the spawnable enemies list
        int spawnWithID = RandomEnemyID();

        //First check if we have free instance of specified enemy
        if (deactiveEnemies.Count != 0)
        {
            bool foundMatch = false;
            for (int i = 0; i < deactiveEnemies.Count; i++)
            {
                //Found match
                if (deactiveEnemies[i].GetID() == spawnWithID)
                {
                    //Set index to choose from deactive list
                    chosenSpawnIndex = i;
                    foundMatch = true;
                }
            }
            if (!foundMatch)
            {
                //No match in free ones, spawn new one
                InstantiateNewEnemy(spawnWithID);
            }
        }
        else
        {
            //No deactive enemies, spawn new one
            InstantiateNewEnemy(spawnWithID);
        }

        //Activate enemy
        deactiveEnemies[chosenSpawnIndex].Activate(NewRandomPos());
        //Add enemy to active list
        activeEnemies.Add(deactiveEnemies[chosenSpawnIndex]);
        //Remove enemy from deactive list
        deactiveEnemies.RemoveAt(chosenSpawnIndex);

    }

    void InstantiateNewEnemy(int spawnWithID) {
        //Instantiate new enemy
        GameObject newEnemy = Instantiate(enemiesToSpawn[spawnWithID].prefab);
        //Get the enemy script
        Enemy enemyScript = newEnemy.GetComponent<Enemy>();
        //Set up the enemy
        enemyScript.SetUp(this, spawnWithID);
        //Add to the list
        deactiveEnemies.Add(enemyScript);
        //Set spawn index
        chosenSpawnIndex = deactiveEnemies.IndexOf(enemyScript);
    }

    //Calculates total weight used for spawning enemies
    void CalculateNewTotalWeighForSpawn() {
        currentTotalSpawnWeight = 0;
        //Go trough all spawnable enemies adn add their spawn chance together
        for (var i = 0; i < enemiesToSpawn.Length; i++)
        {
            currentTotalSpawnWeight += enemiesToSpawn[i].GetCurrentSpawnChance();
        }
    }

    //Calculates ravdom value within total spawn weight
    //Returns chosen enemys index in the spawnable enemies list
    public int RandomEnemyID()
    {
        int randomValue = Random.Range(0, currentTotalSpawnWeight);
        int top = 0;
        for (var i = 0; i < enemiesToSpawn.Length; i++)
        {
            //Add this enemys spawn chance to current total
            top += enemiesToSpawn[i].GetCurrentSpawnChance();
            //If we are under this value choose this enemy to be spawned
            if (randomValue < top)
            {
                return i;
            }
        }
        //We did not get match, spawn firs enemy in the list
        return 0;
    }

    //Calculate new random position to spawn enemy
    Vector2 NewRandomPos() {
        float xPos = Random.Range(spawnXLimit.x, spawnXLimit.y);
        float yPos = Random.Range(spawnYLimit.x, spawnYLimit.y);
        Vector2 newPos = new Vector2(xPos, yPos);

        return newPos;
    }

    public void DeactivateEnemy(Enemy enemyToDeactivate)
    {
        //Remove enemy from active list
        activeEnemies.Remove(enemyToDeactivate);
        //Add enemy to deactive list
        deactiveEnemies.Add(enemyToDeactivate);
    }
}
