﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

    [SerializeField]
    private Vector2 yLimit = new Vector2(-3, 3);

    public List<LevelFragment> levelFragments;

    private float fragmentLenght = 12f; //How wide the fragment is

    private int firstFragmentIndex = 0;
    private int middleFragmentIndex = 2;
    private int lastFragmentIndex = 4;

    private EnemySpawner eSpawner;
    private HumanSpawner hSpawner;

    // Use this for initialization
    void Start () {
        eSpawner = gameObject.GetComponent<EnemySpawner>();
        hSpawner = gameObject.GetComponent<HumanSpawner>();
        SetUp();
    }

    void SetUp() {
        for (int i = 0; i < levelFragments.Count; i++)
        {
            //Calculate starting position for fragment
            float newXPos = fragmentLenght * (-2 + i);
            //Set up fragment
            levelFragments[i].Setup(newXPos, this);
        }
        SetNewSpawnLimits();
        hSpawner.SetUp();
        eSpawner.SetUp();
    }

    //Set spawning limits to be within level
    void SetNewSpawnLimits() {
        float minX = levelFragments[firstFragmentIndex].GetCurrentPos();
        float maxX = levelFragments[lastFragmentIndex].GetCurrentPos();
        Vector2 xLimit = new Vector2(minX, maxX);
        //Tell the limits to the spawner
        eSpawner.SetSpawnLimits(xLimit, yLimit);
    }

    //Loops level fragments
    public void MoveFragments(LevelFragment enteredFragment) {
        //Get the index of entered fragment
        int enteredIndex = levelFragments.IndexOf(enteredFragment);
        //If we are going forward
        if (enteredIndex == CalcIndexValue(middleFragmentIndex, 1))
        {
            levelFragments[firstFragmentIndex].SetNewPosition(levelFragments[lastFragmentIndex].GetCurrentPos()+fragmentLenght);
            SetNewIndexValues(1);
        }
        //If we are going backwards
        else if (enteredIndex == CalcIndexValue(middleFragmentIndex, -1))
        {
            levelFragments[lastFragmentIndex].SetNewPosition(levelFragments[firstFragmentIndex].GetCurrentPos() - fragmentLenght);
            SetNewIndexValues(-1);
        }
        //Level changed, calc new spawn limits
        SetNewSpawnLimits();
    }

    //Set new index values so we can regonize fragment order
    void SetNewIndexValues(int valueChange) {
        firstFragmentIndex = CalcIndexValue(firstFragmentIndex, valueChange);
        middleFragmentIndex = CalcIndexValue(middleFragmentIndex, valueChange);
        lastFragmentIndex = CalcIndexValue(lastFragmentIndex, valueChange);
    }

    //Calculates index values and keeps them inside list values
    int CalcIndexValue(int currentValue, int valueToAdd) {
        int newValue = currentValue + valueToAdd;
        if (newValue < 0)
        {
            newValue = levelFragments.Count - 1;
        }
        else if (newValue > levelFragments.Count - 1)
        {
            newValue = 0;
        }
        return newValue;
    }
}
