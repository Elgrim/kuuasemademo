﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyHandler : MonoBehaviour {

    public delegate void DifficultyChanged();
    public static event DifficultyChanged OnDifficultyIncreased;

    private int enemiesKilled;
    private float timePassed;
    [SerializeField]
    private float startTimeTreshold = 10f;
    [SerializeField]
    private float timeTresholdIncrease = 10f;
    private float currentTimeTreshold;
    [SerializeField]
    private int startKillTreshold = 10;
    [SerializeField]
    private int killTresholdIncrease = 10;
    private int currentKillTreshold;

    private float counterTickSpeed = 1f;

	// Use this for initialization
	void Start () {
        SetUp();
	}

    void SetUp() {
        currentTimeTreshold = startTimeTreshold;
        currentKillTreshold = startKillTreshold;
        StartCoroutine(TimeCounter());
    }

    IEnumerator TimeCounter()
    {
        while (true)
        {
            timePassed += counterTickSpeed;

            if (timePassed >= currentTimeTreshold)
            {
                //Enough time has passed, increase difficulty
                IncreaseDifficulty();
            }

            yield return new WaitForSeconds(counterTickSpeed);
        }
    }

    void EnemyKilled(int score) {
        enemiesKilled++;
        if (enemiesKilled >= currentKillTreshold)
        {
            //Enough enemies were killed, increase difficulty
            IncreaseDifficulty();
        }
    }

    void IncreaseDifficulty() {
        currentTimeTreshold += timeTresholdIncrease;
        currentKillTreshold += killTresholdIncrease;
        if (OnDifficultyIncreased != null) OnDifficultyIncreased();
    }
}
