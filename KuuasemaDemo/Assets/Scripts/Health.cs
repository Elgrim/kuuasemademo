﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    public delegate void HealthChanged(float newHealth);
    public static event HealthChanged OnHealthChanged;

    [SerializeField]
    private float maxHealth;
    private float currentHealth;

    [SerializeField]
    private bool isPlayer;

    private Enemy enemyScript;

    void Start()
    {
        SetUp(); 
    }

    public void SetUp() {
        //Set health to max
        currentHealth = maxHealth;
        if (isPlayer)
        {
            if (OnHealthChanged != null) OnHealthChanged(currentHealth);
        }
        else
        {
            enemyScript = gameObject.GetComponent<Enemy>();
        }
    }

    public void TakeDamage(float damage) {
        currentHealth -= damage;
        if (isPlayer)
        {
            if (OnHealthChanged != null) OnHealthChanged(currentHealth);
        }
        if (currentHealth <= 0)
        {
            //Health dropped below zero
            if (isPlayer)
            {
                GameController.instance.GameOver();
            }
            else
            {
                enemyScript.EnemyDied();
            }
        }
        
    }
}
