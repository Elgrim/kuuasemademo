﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

    [SerializeField]
    private float speed = 1.0f;
    [SerializeField]
    private float speedWithTarget = 2.0f;

    [SerializeField]
    private float distanceToKeep = 1.0f;

    [SerializeField]
    private Vector2 heightConstraints;

    private Vector3 targetPos;


    public void WanderAround()
    {
        if (transform.position != targetPos)
        {
            //We are not yet at the target pos, move towards it
            transform.position = Vector3.MoveTowards(transform.position, targetPos, speed*Time.deltaTime);
        }
        else
        {
            NewRandomTargetPos();
        }
    }

    public void FollowTarget(Transform target) {
        //Calculate current distance to target
        float distance = Vector3.Distance(target.position, transform.position);
        if (distance > distanceToKeep)
        {
            //We are too far, move closer to the target
            transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }
    }

    public void MoveToTarget(Transform target)
    {
        transform.position = Vector3.MoveTowards(transform.position, target.position, speedWithTarget * Time.deltaTime);
    }

    //Calculatess new random pos to move to
    public void NewRandomTargetPos()
    {
        float x = transform.position.x + Random.Range(-7, 7);
        float y = Random.Range(heightConstraints.x, heightConstraints.y);

        targetPos = new Vector3(x,y,0);
    }

}
