﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyType {
    Shooter, Bomber, Snatcher
}

public class Enemy : MonoBehaviour {

    delegate void EnemyAction();
    EnemyAction onEnemyAction;
    EnemyAction onEnemyActivation;

    [SerializeField]
    private int scoreAmount = 100;

    public EnemyType type;

    private int currentID;

    private Health healthScript;
    private EnemySpawner enemySpawner;

    private EnemyType_Snatcher snatcherScript;
    private EnemyType_Bomber bomberScript;
    private EnemyType_Shooter shooterScript;

    private GameController gameController;

    void Update()
    {
        if(onEnemyAction != null) onEnemyAction();
    }

    public void SetUp(EnemySpawner spawner, int id)
    {
        //Set up references
        currentID = id;
        enemySpawner = spawner;
        healthScript = gameObject.GetComponent<Health>();
        gameController = GameController.instance;
        SetEnemyAction();
    }

    public void Activate(Vector2 newPos) {
        //Set new position
        transform.position = newPos;
        //Set up health
        healthScript.SetUp();
        //Send activation call to selected type
        ActivateByType();
        //Activate this object
        gameObject.SetActive(true);
    }

    //Set action for selected type
    void SetEnemyAction()
    {
        switch (type)
        {
            case EnemyType.Shooter:
                shooterScript = gameObject.GetComponent<EnemyType_Shooter>();
                shooterScript.SetUp();
                onEnemyAction += ShooterAction;
                break;
            case EnemyType.Bomber:
                bomberScript = gameObject.GetComponent<EnemyType_Bomber>();
                bomberScript.SetUp();
                onEnemyAction += BomberAction;
                break;
            case EnemyType.Snatcher:
                snatcherScript = gameObject.GetComponent<EnemyType_Snatcher>();
                snatcherScript.SetUp();
                onEnemyAction += SnatcherAction;
                break;
            default:
                break;
        }
    }

    //Set action for selected type
    void ActivateByType()
    {
        switch (type)
        {
            case EnemyType.Shooter:
                shooterScript.Activate();
                break;
            case EnemyType.Bomber:
                bomberScript.Activate();
                break;
            case EnemyType.Snatcher:
                snatcherScript.Activate();
                break;
            default:
                break;
        }
    }

    //Call for shooter actions
    void ShooterAction() {
        shooterScript.Action();
    }

    //Call for bomber actions
    void BomberAction()
    {
        bomberScript.Action();
    }

    //Call for Snatcher actions
    void SnatcherAction()
    {
        snatcherScript.Action();
    }

    //Called when object dies
    public void EnemyDied() {
        //Tell the spawner this object has died
        enemySpawner.DeactivateEnemy(this);
        //Send death delegate to count score
        gameController.AddScore(scoreAmount);
        gameObject.SetActive(false);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //If we collided with player
        if (collision.transform.tag == "Player")
        {
            //Deal damage
            collision.transform.GetComponentInParent<Health>().TakeDamage(50);
            //Deactivate this object
            gameObject.SetActive(false);
        }
    }

    public int GetID() {
        return currentID;
    }
}
