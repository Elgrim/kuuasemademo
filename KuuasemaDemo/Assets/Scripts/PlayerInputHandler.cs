﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputHandler : MonoBehaviour {

    private PlayerMovement movementScript;
    private Shoot shootScript;
    private Camera mainCamera;
    private bool isAutocombatActive;

    private void Start()
    {
        mainCamera = Camera.main;
        movementScript = gameObject.GetComponent<PlayerMovement>();
        shootScript = gameObject.GetComponent<Shoot>();
    }

    void Update()
    {
        if (!isAutocombatActive)
        {
            CheckForInputs();
        }
       
    }

    public void AutocombatStatus(bool status) {
        isAutocombatActive = status;
    }

    void CheckForInputs()
    {

        if (Input.GetButton("Vertical"))
        {
            movementScript.AddVertical(Input.GetAxis("Vertical"));
        }

        if (Input.GetButton("Horizontal"))
        {
            movementScript.AddHorizontal(Input.GetAxis("Horizontal"));
        }

        if (Input.GetButton("Fire1"))
        {
            Vector2 shootDir = transform.right;
            if (movementScript.shipSprite.flipX == true)
            {
                shootDir = -transform.right;
            }
            shootScript.ShootProjectile(shootDir, GunType.Main);
        }
        if (Input.GetButton("Fire2"))
        {
            //Calculate dir where to shoot
            Vector3 shootDir = (GetMousePos() - transform.position).normalized;
            shootScript.ShootProjectile(shootDir, GunType.Auxillary);
        }

    }

    Vector3 GetMousePos() {
        Vector3 mousePos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        //Make sure that mouse pos z value is zero
        mousePos = new Vector3(mousePos.x, mousePos.y, 0);
        return mousePos;
    }
}
