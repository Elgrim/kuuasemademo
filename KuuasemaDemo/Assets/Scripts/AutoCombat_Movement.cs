﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoCombat_Movement : MonoBehaviour {


    private AutoCombat mainScript;
    private AutoCombat_Detection ac_Detection;
    private PlayerMovement pMovement;

    public void SetUp(AutoCombat acScript)
    {
        mainScript = acScript;
        pMovement = mainScript.player.GetComponent<PlayerMovement>();
        ac_Detection = gameObject.GetComponent<AutoCombat_Detection>();
    }

    //Make player face target direction
    public void FaceTargetDir(Transform target)
    {
        //Are we facing target alredy
        if (!ac_Detection.IsEnemyInTheFaceDirection(target))
        {
            //We need to face target
            float value = 1;
            //Is target behind us on x axis
            if (!ac_Detection.IsEnemyAhead(target))
            {
                value = -1;
            }
            MoveHorizontally(value);
        }
    }

    //Move player vertically
    public void MoveVertically(float value)
    {
        pMovement.AddVertical(value);
    }

    //Move player horizontally
    public void MoveHorizontally(float value)
    {
        pMovement.AddHorizontal(value);
    }

    //Make player move to same level as target
    //Offset chanches how much abowe or below target we set goal
    public void MoveToTargetLevel(Transform target, float offset)
    {
        //Check if player needs to move on y axis
        if (target.position.y + offset != transform.position.y)
        {
            if (target.position.y + offset <= transform.position.y)
            {
                //Move down
                MoveVertically(-1);
            }
            else
            {
                //move up
                MoveVertically(1);
            }
        }
    }

    //Make player move to the target
    //Offset chanches how much ahead or before target we set goal
    public void MoveToTargetDir(Transform target, float offset)
    {
        //Check if player needs to move on x axis
        if (target.position.x + offset != transform.position.x)
        {
            if (target.position.x + offset <= transform.position.x)
            {
                //Move down
                MoveHorizontally(-1);
            }
            else
            {
                //move up
                MoveHorizontally(1);
            }
        }
    }

}

