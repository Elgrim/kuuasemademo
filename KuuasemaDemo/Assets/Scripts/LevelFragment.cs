﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelFragment : MonoBehaviour {

    private LevelManager levelManager;

    public Transform captureZone;

    public void Setup(float posX, LevelManager lvlMan) {
        levelManager = lvlMan;
        transform.position = new Vector3(posX, 0, 0);
    }

    public void SetNewPosition(float posX) {
        transform.position = new Vector3(posX, 0, 0);
    }

    public float GetCurrentPos() {
        return transform.position.x;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            //Player entered are, move level fragments
            levelManager.MoveFragments(this);
        }
        else if (other.tag == "Enemy" || other.tag == "Human")
        {
            //Object entered area, make it child so it moves with fragment
            other.transform.parent.SetParent(transform);
            
        }
    }
}
