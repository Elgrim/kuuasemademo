﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIUpdater : MonoBehaviour {

    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI hiScoreText;
    public TextMeshProUGUI healthText;
    public TextMeshProUGUI humanText;

    public GameObject gameOverWindow;

    public AutoCombat ac_script;
    public TextMeshProUGUI acText;

    private int humanCount;

    void OnEnable()
    {
        ScoreManager.OnScoreIncrease += UpdateScore;
        ScoreManager.OnHiScoreIncrease += UpdateHiScore;
        Health.OnHealthChanged += UpdateHealth;
        GameController.OnGameOver += ShowGameOverWindow;
        Human.OnHumanSpawned += AddToHumanCount;
        Human.OnHumanDeath += RemoveFromHumanCount;
    }

    void OnDisable()
    {
        ScoreManager.OnScoreIncrease -= UpdateScore;
        ScoreManager.OnHiScoreIncrease -= UpdateHiScore;
        Health.OnHealthChanged -= UpdateHealth;
        GameController.OnGameOver -= ShowGameOverWindow;
        Human.OnHumanSpawned -= AddToHumanCount;
        Human.OnHumanDeath -= RemoveFromHumanCount;
    }

    void AddToHumanCount() {
        humanCount++;
        UpdateHumanCount();
    }

    void RemoveFromHumanCount()
    {
        humanCount--;
        UpdateHumanCount();
    }

    void UpdateHumanCount()
    {
        humanText.text = "x" + humanCount;
    }

    void UpdateScore(int newScore)
    {
        scoreText.text = "Score: " + newScore;
    }

    void UpdateHiScore(int newScore)
    {
        hiScoreText.text = "Best: " + newScore;
    }

    void UpdateHealth(float currentHealth)
    {
        healthText.text = "Health: " + currentHealth;
    }

    void ShowGameOverWindow() {
        gameOverWindow.SetActive(true);
    }

    public void RestartButtonPressed() {
        GameController.instance.RestartGame();
    }

    public void ToggleAutoCombat() {
        bool currentStatus = ac_script.IsAutoCombatActive();
        ac_script.ToggleAutoCombat(!currentStatus);
        if (currentStatus)
        {
            acText.text = "Auto Combat: off";
            acText.color = Color.red;
        }
        else
        {
            acText.text = "Auto Combat: on";
            acText.color = Color.green;
        }
    }
}
