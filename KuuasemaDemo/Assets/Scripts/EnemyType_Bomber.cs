﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyType_Bomber : MonoBehaviour {

    private EnemyMovement movement;
    private Transform target;

    void OnDisable()
    {
        //When ever object gets disabled call die function
        ObjectDied();
    }

    public void SetUp()
    {
        movement = gameObject.GetComponent<EnemyMovement>();
    }

    public void Activate()
    {
        //Set new random position to move to
        movement.NewRandomTargetPos();
    }

    public void Action()
    {
        if (target)
        {
            Kamikaze();
        }
        else
        {
            //No target so move randomly
            movement.WanderAround();
        }
    }

    void Kamikaze() {
        movement.MoveToTarget(target);
    }

    //Called when object dies
    void ObjectDied()
    {
        //Clear target reference
        target = null;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            //Found new target
            target = other.transform;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            //Target got away, calc new random pos to move to
            movement.NewRandomTargetPos();
            //Clear target reference
            target = null;
        }
    }
}
