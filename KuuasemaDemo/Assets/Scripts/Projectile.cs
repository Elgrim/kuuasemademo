﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ValidTargets
{
    Player, Enemy
}

public class Projectile : MonoBehaviour {

    //Projectile values
    [SerializeField]
    private float speed = 1000f;
    [SerializeField]
    private float damage = 10f;
    //Projectile values
    private ValidTargets targetTag;
    private GunType gunTypeShotFrom; //Used to determine gun type that was used to shoot this projectile

    //Life time values
    [SerializeField]
    private float lifeTime = 2.0f;
    private float lifeTimeTickSpeed = 0.1f;
    private float currentLifeTime = 0f;

    //References
    private Shoot shootScript;
    private Rigidbody2D rb;

    public void SetUp(Shoot shooter, GunType newType) {
        gunTypeShotFrom = newType;
        //Set references if they are not yet set
        rb = gameObject.GetComponent<Rigidbody2D>();
        shootScript = shooter;
        targetTag = shooter.targetTag;
    }

    public void ShootToDirection(Vector3 shootDir)
    {
        SetPosition();
        gameObject.SetActive(true);
        StartCoroutine(TickLifeTime());
        //Make the projectile face shooting direction
        transform.right = shootDir;
        //Add force to the rigidbody
        rb.AddForce(transform.right * speed);
    }

    void SetPosition() {
        transform.position = shootScript.projectilePos.position;
        transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    IEnumerator TickLifeTime()
    {
        //Set life time to max
        currentLifeTime = lifeTime;
        while (true)
        {
            currentLifeTime -= lifeTimeTickSpeed;

            if (currentLifeTime <= 0)
            {
                //Life time has tun out, deactivate projectile
                Deactivate();
            }

            yield return new WaitForSeconds(lifeTimeTickSpeed);
        }
    }

    //Called after projectile collides or its life time has run out
    void Deactivate() {
        StopAllCoroutines();
        gameObject.SetActive(false);
        //Send deactivation call to the shooter
        shootScript.DeactivateProjectile(this);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == targetTag.ToString())
        {
            if(other.GetComponentInParent<Health>()) other.GetComponentInParent<Health>().TakeDamage(damage);
            //We hit something, deactivate projectile
            Deactivate();

        }
    }

    //Returns guntype that was used to shoot this projectile
    public GunType GetProjectileType() {
        return gunTypeShotFrom;
    }
}
