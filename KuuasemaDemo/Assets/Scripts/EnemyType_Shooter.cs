﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyType_Shooter : MonoBehaviour
{

    private EnemyMovement movement;
    private Transform target;
    private Shoot shootScript;

    void OnDisable()
    {
        //When ever object gets disabled call die function
        ObjectDied();
    }

    public void SetUp()
    {
        movement = gameObject.GetComponent<EnemyMovement>();
        shootScript = gameObject.GetComponent<Shoot>();
    }

    public void Activate()
    {
        //Set new random position to move to
        movement.NewRandomTargetPos();
    }

    public void Action()
    {
        if (target)
        {
            ShootAndFollowTarget();
        }
        else
        {
            //No target so move randomly
            movement.WanderAround();
        }
    }

    void ShootAndFollowTarget()
    {
        //Shoot
        //Calculate dir where to shoot
        Vector3 shootDir = (target.position - transform.position).normalized;
        shootScript.ShootProjectile(shootDir, GunType.Main);
        //Follow
        movement.FollowTarget(target);
    }

    //Called when object dies
    void ObjectDied()
    {
        //Reset shootscript
        if (shootScript) shootScript.ObjectDied();
        //Clear target reference
        target = null;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            //Found new target
            target = other.transform;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            //Target got away, calc new random pos to move to
            movement.NewRandomTargetPos();
            //Clear target reference
            target = null;
        }
    }
}
