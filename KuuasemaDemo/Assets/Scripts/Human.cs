﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HumanState
{
    Grounded, Kidnapped, Falling, Saved, Dead
}

public class Human : MonoBehaviour {
    public delegate void HumanEvent();
    public static event HumanEvent OnHumanSpawned;
    public static event HumanEvent OnHumanDeath;

    public delegate void HumanTakenEvent(Human humanScript);
    public static event HumanTakenEvent OnHumanKidnapped;

    public HumanState currentState;

    private Transform carrier;

    public Rigidbody2D rb;

    // Update is called once per frame
    void Update () {
        if (currentState == HumanState.Grounded)
        {
           // WanderAround();
        }
        if (currentState == HumanState.Kidnapped || currentState == HumanState.Saved)
        {
            if (carrier == null || !carrier.gameObject.activeInHierarchy)
            {
                OnStateChanged(HumanState.Falling);
            }
            transform.position = carrier.position;
        }
    }

    public void SetUp() {
        
        if (OnHumanSpawned != null) OnHumanSpawned();
    }

    void OnStateChanged(HumanState newState) {
        currentState = newState;
        switch (newState)
        {
            case HumanState.Grounded:
                GameController.instance.AddScore(500);
                rb.gravityScale = 0;
                break;
            case HumanState.Kidnapped:
                if (OnHumanKidnapped != null) OnHumanKidnapped(this);
                rb.gravityScale = 0;
                break;
            case HumanState.Falling:
                rb.gravityScale = 0.2f;
                break;
            case HumanState.Saved:
                rb.gravityScale = 0;
                break;
            case HumanState.Dead:
                if (OnHumanDeath != null) OnHumanDeath();
                gameObject.SetActive(false);
                break;
            default:
                break;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        switch (other.tag)
        {
            case "Ground":
                if (currentState == HumanState.Falling)
                {
                    OnStateChanged(HumanState.Dead);
                }
                else if (currentState == HumanState.Saved)
                {
                    OnStateChanged(HumanState.Grounded);
                    rb.velocity = Vector2.zero;
                }
                break;
            case "Enemy":  
                if (currentState == HumanState.Grounded || currentState == HumanState.Falling)
                {
                    if (other.GetComponentInParent<Enemy>().type == EnemyType.Snatcher)
                    {
                        carrier = other.transform.parent;
                        OnStateChanged(HumanState.Kidnapped);
                    }
                }
                break;
            case "Player":
                if (currentState == HumanState.Falling)
                {
                    carrier = other.transform.parent;
                    OnStateChanged(HumanState.Saved);
                }
                break;
            case "CaptureZone":
                OnStateChanged(HumanState.Dead);
                break;
            default:
                break;
        }

      
    }

    public HumanState GetCurrentState() {
        return currentState;
    }

    public Transform GetCurrentCarrier()
    {
        return carrier;
    }
}
