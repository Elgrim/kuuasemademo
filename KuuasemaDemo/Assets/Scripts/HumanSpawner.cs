﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanSpawner : MonoBehaviour {

    public GameObject humanPrefab;

    [SerializeField]
    private int maxSpawns = 5;

    private Vector2 spawnXLimit = new Vector2(-24, 24);
    private float yPos = -4.5f;



    // Use this for initialization
    public void SetUp()
    {
        for (int i = 0; i < maxSpawns; i++)
        {
            SpawnHuman();
        }
    }


    void SpawnHuman()
    {
        //Instantiate new enemy
        GameObject newHuman = Instantiate(humanPrefab, NewRandomPos(), transform.rotation);
        newHuman.GetComponent<Human>().SetUp();
    }

    //Calculate new random position to spawn enemy
    Vector2 NewRandomPos()
    {
        float xPos = Random.Range(spawnXLimit.x, spawnXLimit.y);
        Vector2 newPos = new Vector2(xPos, yPos);

        return newPos;
    }
}
