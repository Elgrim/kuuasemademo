﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoCombat_Detection : MonoBehaviour {

    private PlayerMovement pMovement; //Used to get ship direction
    private Shoot shootScript;

    //Autocombat scripts
    private AutoCombat mainScript;
    private AutoCombat_Movement ac_movement;
    private AutoCombat_EnemyHandler ac_EnemyHandler;

  

    public void SetUp(AutoCombat acScript)
    {
        mainScript = acScript;
        pMovement = mainScript.player.GetComponent<PlayerMovement>();
        ac_EnemyHandler = gameObject.GetComponent<AutoCombat_EnemyHandler>();
        shootScript = mainScript.player.GetComponent<Shoot>();
    }


    //Checks if we have any enemies that we can shoot with main weapon
    public void CheckIfEnemiesInFrontOfTheShip()
    {
        List<Enemy> enemyList = ac_EnemyHandler.GetEnemyList();
        //bool isFlipped = pMovement.shipSprite.flipX;
        for (int i = 0; i < enemyList.Count; i++)
        {
            if (enemyList[i] != null)
            {
                //Is any of the enemies directly in front of us
                if (enemyList[i].transform.position.y <= transform.position.y + 0.5f && enemyList[i].transform.position.y >= transform.position.y - 0.5f)
                {
                    //Is our ship flipped (facing backwards
                    bool isFlipped = IsShipFlipped();
                    //Is the enemy in the direction we are facing
                    if (IsEnemyInTheFaceDirection(enemyList[i].transform))
                    {
                        //Shoot in the direction we are facing
                        if (!isFlipped)
                        {
                            shootScript.ShootProjectile(transform.right, GunType.Main);
                        }
                        else
                        {
                            shootScript.ShootProjectile(-transform.right, GunType.Main);
                        }
                    }
                }
            }
            

        }
    }

    //Is the enemy in the direction we are facing
    public bool IsEnemyInTheFaceDirection(Transform target)
    {
        bool correctDirection = false;
        bool isFlipped = IsShipFlipped();
        if (target.position.x > transform.position.x && !isFlipped)
        {
            correctDirection = true;
        }
        else if (target.position.x < transform.position.x && isFlipped)
        {
            correctDirection = true;
        }
        return correctDirection;
    }

    //Check if the ship sprite is flipped aka are we going forwards or backwards
    bool IsShipFlipped()
    {
        bool isFlipped = pMovement.shipSprite.flipX;
        return isFlipped;
    }

    //Detect if enemy is ahead of us or not
    public bool IsEnemyAhead(Transform target)
    {
        bool isAhead = true;
        if (target.position.x < transform.position.x)
        {
            isAhead = false;
        }
        return isAhead;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy")
        {
            ac_EnemyHandler.EnemyDetected(other.GetComponentInParent<Enemy>());
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Enemy")
        {
            ac_EnemyHandler.EnemyLost(other.GetComponentInParent<Enemy>());
        }
    }
}
