﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyType_Snatcher : MonoBehaviour {

    private Human humanScript;
    private EnemyMovement movement;
    private Transform target;

    void OnDisable()
    {
        //When ever object gets disabled call die function
        ObjectDied();
    }

    public void SetUp() {
        movement = gameObject.GetComponent<EnemyMovement>();
    }

    public void Activate() {
        //Set new random position to move to
        movement.NewRandomTargetPos();
    }

    public void Action()
    {
        if (target)
        {
            KidnapHuman();
        }
        else
        {
            //No target so move randomly
            movement.WanderAround();
        }
    }

    void KidnapHuman() {
        HumanState state = humanScript.GetCurrentState();
        if (state == HumanState.Grounded || state == HumanState.Falling)
        {
            movement.MoveToTarget(target);
        }
        else
        {
            if (state == HumanState.Kidnapped && humanScript.GetCurrentCarrier() == transform)
            {
                //Take human away
                if (state == HumanState.Dead)
                {
                    //Target died, go find new one
                    target = null;
                }
                movement.MoveToTarget(transform.GetComponentInParent<LevelFragment>().captureZone);
            }
            else
            {
                //Some other snatcher or player got there first, ignore human
                target = null;
            }
        }
    }

    //Called when object dies
    void ObjectDied()
    {
        //Clear target reference
        target = null;
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag == "Human" && target == null)
        {

            HumanState state = other.GetComponentInParent<Human>().GetCurrentState();
            if (state == HumanState.Grounded || state == HumanState.Falling)
            {
                //Found new target
                humanScript = other.GetComponentInParent<Human>();
                target = other.transform;
            }
        }
    }
}
