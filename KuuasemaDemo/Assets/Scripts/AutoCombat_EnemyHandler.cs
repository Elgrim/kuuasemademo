﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoCombat_EnemyHandler : MonoBehaviour {

    private Shoot shootScript;

    //Enemy References
    public List<Enemy> enemiesCloseBy;
    private Enemy targetedEnemy;
    private Transform shootTarget;

    //Autocombat scripts
    private AutoCombat mainScript;
    private AutoCombat_Movement ac_Movement;

    public void SetUp(AutoCombat acScript)
    {
        mainScript = acScript;
        ac_Movement = gameObject.GetComponent<AutoCombat_Movement>();
        shootScript = mainScript.player.GetComponent<Shoot>();
    }

    //Get a target and start shooting it
    public void FindAndShootEnemy()
    {
        if (shootTarget)
        {
            //We have a target
            if (shootTarget.gameObject.activeInHierarchy)
            {
                //Target is alive, do something about it

                if (!mainScript.savingHuman)
                {
                    //If we are not saving humans, face enemy and move to same level
                    ac_Movement.FaceTargetDir(shootTarget);
                    ac_Movement.MoveToTargetLevel(shootTarget, 0);
                }

                //Calculate dir where to shoot
                Vector3 shootDir = (shootTarget.position - transform.position).normalized;
                shootScript.ShootProjectile(shootDir, GunType.Auxillary);
            }
            else
            {
                //Target was lost
                //Clear references and remove enemy from the list
                enemiesCloseBy.Remove(targetedEnemy);
                targetedEnemy = null;
                shootTarget = null;
            }
        }
        else
        {
            //Get a new target to shoot at
            GetNewTarget();
        }
    }

    //Find new enemy to target
    void GetNewTarget()
    {
        Transform tempTarget = null;

        for (int i = 0; i < enemiesCloseBy.Count; i++)
        {
            //Check for bombers first to make them #1 priority on enemies
            if (enemiesCloseBy[i].type == EnemyType.Bomber)
            {
                targetedEnemy = enemiesCloseBy[i];
                shootTarget = enemiesCloseBy[i].transform;
                return;
            }
            else
            {
                //No bombers
                //Check for snatchers as #2 priority
                if (enemiesCloseBy[i].type == EnemyType.Snatcher)
                {
                    targetedEnemy = enemiesCloseBy[i];
                    shootTarget = enemiesCloseBy[i].transform;
                    return;
                }
                else
                {
                    //No priority targets set temporary target to this 
                    targetedEnemy = enemiesCloseBy[i];
                    tempTarget = enemiesCloseBy[i].transform;
                }
            }
        }
        //No priority targets set temp target as target
        shootTarget = tempTarget;
    }

    public List<Enemy> GetEnemyList(){
        return enemiesCloseBy;
    }

    public void EnemyDetected(Enemy newEnemy) {
        //Add new enemy to the list
        enemiesCloseBy.Add(newEnemy);
        if (mainScript.targetHumanScript)
        {
            //We are saving human, check if new enemy was the one taking human away
            if (mainScript.targetHumanScript.GetCurrentCarrier() == newEnemy.transform)
            {
                //It is taking the human away, make this new shoot target
                targetedEnemy = newEnemy;
                shootTarget = newEnemy.transform;
            }
        }
    }

    public void EnemyLost(Enemy newEnemy)
    {
        enemiesCloseBy.Remove(newEnemy);
        if (targetedEnemy == newEnemy)
        {
            //Our current target got away
            //Clear references
            targetedEnemy = null;
            shootTarget = null;
        }
    }
}
