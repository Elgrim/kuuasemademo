﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GunType
{
    Main, Auxillary
}

public class Shoot : MonoBehaviour {

    public GameObject mainProjectilePrefab;
    [SerializeField]
    private float mainReloadSpeed; //How fast main gun reloads
    private float mainReload; //Counter for main gun reload
    public GameObject auxillaryProjectilePrefab;
    [SerializeField]
    private float auxillaryReloadSpeed; //How fast aux gun reloads
    private float auxillaryReload; //Counter for aux gun reload
    private GameObject prefabToSpawn; //Reference to the chosen prefab to spawn
    private int chosenSpawnIndex = 0; //Used to determine index value of spawnable projectile in deactive projectiles list

    public Transform projectilePos; //Position where to spawn new projectiles

    public ValidTargets targetTag;
    private float cooldownTickSpeed = 0.02f;
    private bool cooldownCounterActive = false;

    private List<Projectile> activeProjectiles = new List<Projectile>();
    private List<Projectile> deactiveProjectiles = new List<Projectile>();

    public void ShootProjectile(Vector3 targetPos, GunType newType) {
        if (!CheckIfWeHaveChosenGun(newType)) return;
        if (CheckForCooldown(newType))
        {
            //Refresh cooldown
            switch (newType)
            {
                case GunType.Main:
                    mainReload = mainReloadSpeed;
                    break;
                case GunType.Auxillary:
                    auxillaryReload = auxillaryReloadSpeed;
                    break;
            }
            //If cooldown counter is not yet running activate it
            if (!cooldownCounterActive) StartCoroutine(Cooldown());
            
            //Check if we have free instance of specified projectile
            if (deactiveProjectiles.Count != 0)
            {
                bool foundMatch = false;
                for (int i = 0; i < deactiveProjectiles.Count; i++)
                {
                    if (deactiveProjectiles[i].GetProjectileType() == newType)
                    {
                        //Set index to choose from deactive list
                        chosenSpawnIndex = i;
                        foundMatch = true;
                    }
                }
                if (!foundMatch)
                {
                    //No match in free ones, spawn new one
                    Instantiateprojectile(newType);
                }
            }
            else
            {
                //No deactive projectiles, spawn new one
                Instantiateprojectile(newType);
            }
            //Activate projectile
            deactiveProjectiles[chosenSpawnIndex].ShootToDirection(targetPos);

            //Add projectile to active list
            activeProjectiles.Add(deactiveProjectiles[chosenSpawnIndex]);
            //Remove projectile from deactive list
            deactiveProjectiles.RemoveAt(chosenSpawnIndex);

        }
    }


    bool CheckForCooldown(GunType newType) {
        switch (newType)
        {
            case GunType.Main:
                if (mainReload <= 0)
                {
                    return true;
                }
                break;
            case GunType.Auxillary:
                if (auxillaryReload <= 0)
                {
                    return true;
                }
                break;
        }
        return false;
    }

    bool CheckIfWeHaveChosenGun(GunType newType)
    {
        switch (newType)
        {
            case GunType.Main:
                if (mainProjectilePrefab != null)
                {
                    return true;
                }
                break;
            case GunType.Auxillary:
                if (auxillaryProjectilePrefab != null)
                {
                    return true;
                }
                break;
        }
        return false;
    }

    void Instantiateprojectile(GunType newType)
    {
        //Clear reference so we dont accidentally spawn wrong ones
        prefabToSpawn = null;
        //Set prefab to match projectile type chosen
        switch (newType)
        {
            case GunType.Main:
                prefabToSpawn = mainProjectilePrefab;
                break;
            case GunType.Auxillary:
                prefabToSpawn = auxillaryProjectilePrefab;
                break;
        }
        if (prefabToSpawn != null)
        {
            GameObject newProjectile = Instantiate(prefabToSpawn);
            Projectile projectileScript = newProjectile.GetComponent<Projectile>();
            projectileScript.SetUp(this, newType);
            deactiveProjectiles.Add(projectileScript);
            chosenSpawnIndex = deactiveProjectiles.IndexOf(projectileScript);
        }
        
    }

    IEnumerator Cooldown() {
        cooldownCounterActive = true;
        while (true) {
            if (mainReload != 0)
            {
                mainReload -= cooldownTickSpeed;
                if (mainReload <= 0)
                {
                    mainReload = 0;
                }
            }
            if (auxillaryReload != 0)
            {
                auxillaryReload -= cooldownTickSpeed;
                if (auxillaryReload <= 0)
                {
                    auxillaryReload = 0;
                }
            }
            yield return new WaitForSeconds(cooldownTickSpeed);
        }

    }

    public void DeactivateProjectile(Projectile projectileToDeactivate) {
        //Remove projectile from active list
        activeProjectiles.Remove(projectileToDeactivate);
        //Add projectile to deactive list
        deactiveProjectiles.Add(projectileToDeactivate);
    }

    //Called if attached object dies and coroutine should be stopped
    public void ObjectDied() {
        StopAllCoroutines();
        cooldownCounterActive = false;
        mainReload = 0;
        auxillaryReload = 0;
    }
}
