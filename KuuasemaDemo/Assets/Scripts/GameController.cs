﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    public static GameController instance;

    public delegate void GameEvent();
    public static event GameEvent OnGameOver;

    private ScoreManager scoreManager;
    public int hiScore;

    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this) {
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
        }

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        scoreManager = GameObject.FindGameObjectWithTag("ScoreManager").GetComponent<ScoreManager>();
    }

    public void AddScore(int scoreToAdd) {
        scoreManager.AddScore(scoreToAdd);
    }

    public void GameOver() {
        hiScore = scoreManager.HighScore;
        Time.timeScale = 0f;
        if(OnGameOver != null) OnGameOver();
    }

    public void RestartGame()
    {
        scoreManager.ResetScore();
        
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
